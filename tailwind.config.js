module.exports = {
  purge: [],
  darkMode: false,
  theme: {
    extend: {
      zIndex: {
        0.1: "-10",
        0.2: "-20",
        0.3: "-30",
        0.4: "-40",
        0.5: "-50",
      },
      fontFamily: {
        poppins: ["Poppins"],
        shipporiMincho: ["Shippori Mincho"],
      },
      fontSize: {
        "4xs": "0.45rem",
        xxxs: "0.55rem",
        xxs: "0.65rem",
      },
      colors: {
        blue: {
          10: "#ECF4FD",
          50: "#1169D2",
          100: "#102858",
        },
        gray: {
          700: "#DAE1E7",
          800: "#B8C2CC",
        },
        lime: {
          100: "#ECFCCB",
          200: "#D9F99D",
        },
      },
      backgroundImage: (theme) => ({
        "corousel-bg": "url('/images/mainbanner.svg')",
        secondary: "url('/images/secondorybannerbg.svg')",
        "secondary-image": "url('/images/secondorybanner.png')",
        looking1: "url('/images/looking/Group8320.png')",
        looking2: "url('/images/looking/Group8319.png')",
        looking3: "url('/images/looking/Group8318.png')",
        looking4: "url('/images/looking/Group8317.png')",
        tile1: "url('/images/tile-1.png')",
        tile2: "url('/images/tile-2.png')",
        tile3: "url('/images/tile-3.png')",
        tile4: "url('/images/tile-4.png')",
        winter1: "url('/images/winterEssential/image111.png')",
        winter2: "url('/images/winterEssential/image112.png')",
        winter3: "url('/images/winterEssential/image113.png')",
        winter4: "url('/images/winterEssential/image114.png')",
      }),
    },
  },

  variants: {
    extend: {
      placeholderOpacity: ["responsive", "hover", "focus", "active"],
      placeholderColor: ["responsive", "hover", "focus", "active"],
      backgroundColor: ["responsive", "hover", "focus", "active"],
      fontSize: ["responsive", "hover", "focus", "active"],
      padding: ["responsive", "hover", "focus", "active"],
      textColor: ["responsive", "hover", "focus", "active"],
      borderColor: ["responsive", "hover", "focus", "active"],
      borderRadius: ["responsive", "hover", "focus", "active"],
      width: ["responsive", "hover", "focus", "active"],
      fontWeight: ["responsive", "hover", "focus", "active"],
      zIndex: ["responsive", "hover", "focus", "active"],
    },
  },
  plugins: [],
};
