import React from "react";
import BestSellersCard from "../BestSeller/BestSellersCard";
import CardHeading from "../../components/common/CardHeading";
import Carousel from "react-elastic-carousel";
import styles from "../../../styles/Corousel.module.css";

function BestSeller() {
  const BestSellerData = [
    {
      productImage: "/images/product1.png",
      imageButton1: "Triple Immunity",
      imageButton2: "Triple Immunity",
      offer1: "/images/new.png",
      offer2: "/images/bestSeller.png",
      description: "Sunshine Nutrition Thirst Quencher Drink",
      description2: "Ocean Drink",
      rating: "4.8",
      review: "100",
      buyOfferButton: "Buy 2 Get 1",
      instantImage: "/images/instant.png",
      price: "2311.00",
      discountPrice: "5000.00",
      noOfItems: "2",
    },
    {
      productImage: "/images/product1.png",
      imageButton1: "Triple Immunity",
      imageButton2: "Triple ",
      offer1: "/images/new.png",
      offer2: "/images/bestSeller.png",
      description: "Sunshine Nutrition Thirst Quencher Drink",
      description2: "Ocean Drink",
      rating: "4.8",
      review: "100",
      buyOfferButton: "Buy 2 Get 1",
      instantImage: "/images/instant.png",
      price: "2311.00",
      discountPrice: "5000.00",
      noOfItems: "2",
    },
    {
      productImage: "/images/product1.png",
      imageButton1: "Triple Immunity",
      imageButton2: "",
      offer1: "/images/new.png",
      offer2: "/images/bestSeller.png",
      description: "Sunshine Nutrition Thirst Quencher Drink",
      description2: "Ocean Drink",

      rating: "4.8",
      review: "100",
      buyOfferButton: "Buy 2 Get 1",
      instantImage: "/images/instant.png",
      price: "2311.00",
      discountPrice: "5000.00",
      noOfItems: "2",
    },
    {
      productImage: "/images/product1.png",
      imageButton1: "Triple Immunity",
      imageButton2: "Triple Immunity",
      offer1: "/images/new.png",
      offer2: "/images/bestSeller.png",
      description: "Sunshine Nutrition Thirst Quencher Drink",
      description2: "Ocean Drink",

      rating: "4.8",
      review: "100",
      buyOfferButton: "Buy 2 Get 1",
      instantImage: "/images/instant.png",
      price: "2311.00",
      discountPrice: "5000.00",
      noOfItems: "2",
    },
  ];
  return (
    <div>
      {/* Best Sellers for Mobile View  */}
      <div className="  md:border shadow-sm rounded-lg bg-white max-w-screen-xl md:mx-auto px-4 md:px-6 lg:px-8 py-4 ">
        <CardHeading title="Recommended for you " />
        <div className="my-3 md:my-5 flex justify-between gap-3 md:gap-4 lg:gap-6 w-full overflow-x-scroll">
          {BestSellerData.map((sellerItem, index) => (
            <BestSellersCard
              key={index}
              imageButton1={sellerItem.imageButton1}
              imageButton2={sellerItem.imageButton2}
              productImage={sellerItem.productImage}
              offer1={sellerItem.offer1}
              offer2={sellerItem.offer2}
              description={sellerItem.description}
              description2={sellerItem.description2}
              rating={sellerItem.rating}
              review={sellerItem.review}
              buyOfferButton={sellerItem.buyOfferButton}
              instantImage={sellerItem.instantImage}
              price={sellerItem.price}
              noOfItems={sellerItem.noOfItems}
            />
          ))}
        </div>
      </div>
    </div>
  );
}

export default BestSeller;
