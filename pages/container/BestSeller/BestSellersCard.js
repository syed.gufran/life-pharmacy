import React from "react";
import Button from "../../components/common/Button";

function BestSellersCard({
  imageButton1,
  imageButton2,
  productImage,
  offer1,
  offer2,
  description,
  description2,
  rating,
  review,
  buyOfferButton,
  instantImage,
  price,
  discountPrice,
  noOfItems,
}) {
  return (
    <div className="border flex flex-col justify-between rounded-lg w-56 md:w-64 lg:w-64   max-w-sm bg-white ">
      <div className="flex ">
        {/* New and Best seller tags  */}
        <img
          src={offer1}
          alt="new"
          className="h-6 md:h-8 z-10 -ml-2 md:-ml-3 "
        />
        <img
          src={offer2}
          alt="bestSeller"
          className="h-6 md:h-8 z-10 -ml-3 md:-ml-4 "
        />
      </div>
      <div className="p-1.5 md:p-3 flex flex-col justify-between h-full -mt-5 md:-mt-7  ">
        <div className="flex flex-col justify-between border rounded-lg md:min-w-min py-1 md:py-1.5 md:px-1">
          <img
            src={productImage}
            alt="productImage"
            className="h-32 md:h-36 lg:h-44 w-full  p-1 mx-auto"
          />
          <div className="flex justify-between items-center ">
            <div className="flex flex-nowrap  ">
              <Button
                name={imageButton1}
                bgColor="white"
                fontBold="medium"
                textColor="red-600"
                textSize="4xs"
                mdTextSize="xxxs"
                xlTextSize="xxs"
                rounded="sm"
                borderColor="red-500"
                mdMx="0.5"
                mdPx="0.5"
                mdPy="0.5"
                shadow="md"
              />
              <Button
                name={imageButton2}
                mdPx="0.5"
                bgColor="white"
                fontBold="medium"
                textColor="red-600"
                textSize="4xs"
                mdTextSize="xxxs"
                borderColor="red-500"
                mdMx="0.5"
                mdPx="0.5"
                mdPy="0.5"
                shadow="md"
              />
            </div>
            <img src="/images/whiteheart.png" alt="H" className="h-4 md:h-5" />
          </div>
        </div>
        <div className="mt-1">
          <p className=" text-xs font-normal xl:text-base  md:text-sm  text leading-tight text-center text-blue-100 ">
            {description}{" "}
            <span className="hidden md:inline-block">{description2}</span>
          </p>
          <div className="flex flex-col justify-between">
            <div className="flex items-center">
              <img src="/images/star.png" alt="star" className="h-3 md:h-5" />
              <p className="flex items-center text-xxxs md:text-xs lg:text-base  text-blue-50 align-bottom ">
                {rating}{" "}
                <span className="text-gray-400 md:text-xxs md:mx-0.5 ">
                  {" "}
                  ({review} REVIEW)
                </span>
              </p>
            </div>
            <div className="flex justify-between items-center ">
              <div>
                <Button
                  name={buyOfferButton}
                  textSize="xxs"
                  mdTextSize="xs"
                  mdPx="1"
                  textColor="blue-100"
                  fontBold="medium"
                  borderRadius="none"
                  px="0.5"
                  borderColor="blue-100"
                />
              </div>
              <img
                src={instantImage}
                alt="instant"
                className="h-5 md:h-7 lg:h-8 "
              />
            </div>
          </div>
        </div>
      </div>
      <div className="flex  justify-between items-center gap-2 md:gap-0 w-full bg-blue-10 px-2 md:px-3 py-1 md:py-4">
        <div className="w-1/3 md:w-1/2 text-blue-100">
          <p className="text-xxxs md:text-xs md:flex md:items-baseline  leading-none md:leading-3 font-medium ">
            AED <br />{" "}
            <span className=" text-xs md:text-lg font-bold md:leading-none md:mx-1 ">
              {price}
            </span>
          </p>
          <p className="text-xxxs md:text-xs line-through  ">
            AED {discountPrice}
          </p>
        </div>
        <div className="w-2/3 md:w-1/2 flex justify-between items-center ">
          <div>
            <Button
              name="-"
              bgColor="white"
              px="2.5"
              mdPx="3"
              py="0.5"
              textColor="blue-50"
              borderColor="blue-50"
              rounded="none"
              mdTextSize="lg"
            />
          </div>
          <p className="text-xxs mx-2.5 md:text-base text-blue-100 font-semibold ">
            {noOfItems}
          </p>
          <div>
            <Button
              name="+"
              bgColor="blue-50"
              px="2"
              mdPx="2.5"
              py="0.5"
              textColor="white"
              borderColor="blue-50"
              rounded="none"
              mdTextSize="lg"
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default BestSellersCard;
