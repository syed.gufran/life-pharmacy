import React from "react";

function MoreDetailsButton() {
  return (
    <div className="w-11/12 max-w-screen-xl mx-auto my-4 md:my-8 lg:my-10 ">
      <button className="text-blue-100 bg-white font-medium w-full flex flex-row items-center truncate focus:outline-none border  shadow hover:shadow-none focus:shadow-none duration-300 rounded-lg py-4  px-2 text-xs md:text-sm md:p-4 lg:text-base lg:p-6">
        <img src="/images/plus.png" alt="plus" className="h-5 mr-1" />
        More Details about Health, Nutrition &#38; Body Building Supplements
      </button>
    </div>
  );
}

export default MoreDetailsButton;
