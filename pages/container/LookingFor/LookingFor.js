import React from "react";
import CareProductsCard from "../../components/CareProductsCard";
import Button from "../../components/common/Button";

function LookingFor() {
  const CareProductsData = [
    { title: "Beauty Care", bgColor: "looking1" },
    { title: "Nutrition &  Vitamins", bgColor: "looking2" },
    { title: "Keto Diet", bgColor: "looking3" },
    { title: "Personal Care", bgColor: "looking4" },
    { title: "Beauty Care", bgColor: "looking1" },
    { title: "Nutrition & Vitamins", bgColor: "looking2" },
    { title: "Keto Diet", bgColor: "looking3" },
    { title: "Personal Care", bgColor: "looking4" },
  ];

  return (
    <div>
      <div className="md:border shadow-sm rounded-lg bg-white max-w-screen-xl md:mx-auto px-4 md:px-6 lg:px-8 py-4 ">
        <div className="flex justify-between w-full mb-4">
          <h1 className="font-medium text-blue-100 md:text-xl lg:text-2xl xl:text-3xl">
            What are you looking for?
          </h1>
          <div className=" md:hidden">
            <Button
              name="View All"
              bgColor="blue-500"
              textColor="white"
              textSize="xs"
              hovertextSize="xl"
              px="1"
            />
          </div>
        </div>
        <div className="grid grid-cols-2 gap-4 w-full md:grid-cols-4 md:gap-2 lg:gap-7 ">
          {CareProductsData.map((item, index) => (
            <div key={index} className="h-28 lg:h-36 xl:h-40 shadow-sm">
              <CareProductsCard title={item.title} bgColor={item.bgColor} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default LookingFor;
