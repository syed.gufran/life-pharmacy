import React from "react";
import Features from "../../components/Features";
import FeatureCard from "./FeatureCard";

function FeatureBlock() {
  const FeatureCardsData = [
    {
      title: "Instant Delivery",
      description: "Medicine Delivered in 30 Minutes",
      button: "Save Now",
      fromColor: "yellow-500",
      toColor: "yellow-600",
      bgColor: "tile1",
    },
    {
      title: "DSF Deals",
      description: "Up to 90% Off on your favourite brands",
      button: "View Product",
      fromColor: "blue-500",
      toColor: "blue-600",
      bgColor: "tile2",
    },
    {
      title: "Organic Store",
      description: "100% Organic Products",
      button: "View Product",
      fromColor: "green-500",
      toColor: "green-600",
      bgColor: "tile3",
    },
    {
      title: "Female Care",
      description: "We Provide Feminine Hygiene",
      button: "View Product",
      fromColor: "pink-400",
      toColor: "pink-600",
      bgColor: "tile4",
    },
  ];
  const FeaturesData = [
    {
      img: "/images/gifticon.png",
      title: "Free Delivery",
      description: "For all order over AED 100",
    },
    {
      img: "/images/deliveryicon.png",
      title: "Express Shipping",
      description: "Express Shipping",
    },
    {
      img: "/images/securityicon.png",
      title: "Secure Payments",
      description: "For all order over AED 100",
    },
    {
      img: "/images/callicon.png",
      title: "24/7 Support",
      description: "Delicated Support",
    },
  ];

  return (
    <div>
      {/*Free Delivery,Express Shipping,Secure Payments, 24/7 Support  */}
      <div className="hidden md:block md:flex md:flex-row justify-center border-2 shadow-sm bg-white divide-x divide-gray-100 rounded-lg w-full md:mx-auto my-4 md:my-8 px-2 max-w-screen-xl ">
        {FeaturesData.map((item, index) => (
          <div key={index} className="md:w-1/4">
            <Features
              title={item.title}
              description={item.description}
              img={item.img}
            />
          </div>
        ))}
      </div>

      {/*Instant Delivery,DSF Deals,Organic store, female care tiles  */}
      <div className="flex flex-row lg:justify-center  overflow-x-scroll my-2 md:my-6 lg:my-8 max-w-screen-xl lg:mx-auto ">
        {FeatureCardsData.map((item, index) => (
          <div
            key={index}
            class="w-full md:w-1/4 lg:w-11/12 mx-1 xl:mx-0.5 h-14 md:h-28 lg:h-36 xl:h-44"
          >
            <FeatureCard
              title={item.title}
              description={item.description}
              buttonName={item.button}
              bgColor={item.bgColor}
              fromColor={item.fromColor}
              toColor={item.toColor}
            />
          </div>
        ))}
      </div>
    </div>
  );
}

export default FeatureBlock;
