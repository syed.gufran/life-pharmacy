import React from "react";
import classnames from "classnames";
import Button from "../../components/common/Button";

function FeatureCard({
  title,
  description,
  buttonName,
  fromColor,
  viaColor,
  toColor,
  bgColor,
}) {
  return (
    <div
      className={classnames(
        `from- via- to- bg-${bgColor} bg-cover rounded md:rounded-lg bg-gradient-to-r shadow-sm flex  md:flex-col md:justify-between lg:mx-auto py-2 md:py-4 lg:py-5 px-2 pr-10 md:px-4 lg:px-6 w-full  h-full`
      )}
    >
      {/* As of now no data as we are using images to add data just pass props data as similar  */}
      {/* fromColor, toColor, viaColor, title, descripition,  */}

      <div className=" text-white lg:mb-10">
        <h5 className="text-xs leading-none font-medium lg:text-lg w-3/5 md:w-full ">
          {}
        </h5>
        <p className="hidden md:block text-xs font-thin flex flex-nowrap  ">
          {}
        </p>
      </div>
      <div className="hidden md:block ">
        {/* <Button name={buttonName} py="1" rounded="rounded-sm" bgColor="white" textColor={fromColor} mdWidth="1/2"  lgWidth="2/5" textSize="xs" hovertextSize="xl"  mdPx="0" /> */}
      </div>
    </div>
  );
}

export default FeatureCard;
