import React from 'react'

function FeaturedOfferCards({title}) {
    return (
        <div >
            <img src="" alt="" className="h-24 border rounded-lg border-blue-100 object-cover bg-white" />
            <h1 className="text-center text-xs text-blue-100 my-1">{title}</h1>
        </div>
    )
}

export default FeaturedOfferCards
