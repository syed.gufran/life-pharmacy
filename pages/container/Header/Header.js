import React, { useState, useEffect } from "react";
import Dropdown from "./Dropdown";
import OfferBlock from "./OfferBlock";
import Search from "./Search";

function Header() {
  const OfferData = [
    {
      title: "LIMITED PERIOD OFFER",
      bgColor: "yellow-100",
    },
    {
      title: "LIMITED PERIOD OFFER",
      bgColor: "yellow-50",
    },
    {
      title: "LIMITED PERIOD OFFER",
      bgColor: "green-50",
    },
    ,
    {
      title: "LIMITED PERIOD OFFER",
      bgColor: "green-100",
    },
    {
      title: "LIMITED PERIOD OFFER",
      bgColor: "green-200",
    },
  ];

  const [searchbar, showSearchBar] = useState(false);

  const hideSearchBar = () => {
    if (window.scrollY >= 40) {
      showSearchBar(true);
    } else {
      showSearchBar(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", hideSearchBar);
  }, []);

  return (
    <>
      <div className="bg-white z-20 static mt-9 md:mt-0 md:sticky md:top-0 rounded-bl-md rounded-br-md shadow-sm transition-shadow border-b border-gray-200  ">
        <div className="flex justify-between items-center max-w-screen-xl  mx-auto overflow-y-scroll bg-white">
          <div className="md:mx-auto w-full md:flex md:flex-row  max-w-screen-xl ">
            {OfferData.map((item, index) => (
              <div className="md:w-30 lg:mx-1.5  " key={index}>
                <OfferBlock title={item.title} bgColor={item.bgColor} />
              </div>
            ))}
          </div>
          <div className="hidden md:block  ">
            <div className=" flex items-center text-blue-100">
              <button className=" flex flex-nowrap items-center rounded text-xxs bg-gray-100 px-2 py-0.5 font-bold mx-1 shadow focus:outline-none hover:shadow-none transition-transform">
                {" "}
                AR <i class="fas fa-chevron-down ml-1 text-xxs"></i>{" "}
              </button>
              <button className="flex items-center rounded text-xxs bg-gray-100 px-2 py-0.5 font-bold mx-1 shadow focus:outline-none hover:shadow-none transition-transform ">
                {" "}
                BAHRAIN <i className="fas fa-chevron-down ml-1 text-xxs"></i>{" "}
              </button>
            </div>
          </div>
        </div>
        <div className=" md:border-t  md:border-b border-gray-100 ">
          <Search />
        </div>
        <Dropdown />
      </div>

      {/* Search bar viewed on scroll in mobile */}
      <div className={searchbar ? "search-bar-active" : "search-bar"}>
        <form method="GET" className="  w-full  ">
          <div className="relative mt-4  w-full shadow  rounded-md ">
            <span className="absolute inset-y-0 left-0 flex items-center pl-1 md:pl-3 md:pr-1 rounded-md   ">
              <button
                type="submit"
                className=" focus:outline-none focus:shadow-outline   rounded-md"
              >
                <svg
                  fill="none"
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  viewBox="0 0 24 24"
                  className="w-5 h-5"
                >
                  <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                </svg>
              </button>
            </span>
            <input
              type="search"
              className="md:hidden text-sm text-blue-900 focus:text-blue-900 cursor-pointer bg-gray-50 rounded-md pl-7 md:pl-10 py-2.5 lg:hover:pl-11  transformation transition duration-500  md:py-1 lg:py-1.5 md:text-base h-full shadow focus:outline-none font-normal w-full placeholder-gray-600 "
              placeholder="Search Medicine &#38; Healthcare Products"
              autoComplete="off"
            />
          </div>
        </form>
      </div>
    </>
  );
}

export default Header;
