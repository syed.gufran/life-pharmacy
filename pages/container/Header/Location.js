import React from "react";

const Location = () => {
  return (
    <div className="block md:hidden w-56 flex items-center overflow-hidden border-dotted border-b-2 border-blue-50 text-xs   pb-0.5 text-blue-800 ">
      <i class="fas fa-map-marker-alt text-xxs mr-2"></i>
      <h5 className="font-medium"> Marasi Drive , Business Bay, Dubai</h5>
    </div>
  );
};

export default Location;
