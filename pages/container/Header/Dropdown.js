import { useState } from "react";
import Button from "../../components/common/Button";

function Dropdown() {
  const [dropdownOpen, setdropDown] = useState(false);
  const [sublist, setSublist] = useState(false);
  const [type, setType] = useState("");
  const [mainItem, setMainItem] = useState("");
  const dropdownContent = [
    { title: "Body Care" },
    { title: "Sports Nutrition" },
    { title: "Nutrition & suplement" },
    { title: "Home Healthcare" },
    { title: "Mother & babycare" },
    { title: "Personal Care" },
    { title: "Service program" },
  ];
  const dropDown = [
    {
      name: "Categories",
      bgColor: "blue-400",
      image: "",
      svg: (
        <svg
          className="h-6 w-5 "
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      ),
      invertedsvg: (
        <svg
          className="h-6 w-5 transform rotate-180"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      ),
    },
    {
      name: "Brands",
      bgColor: "blue-400",
      image: "/images/icon 3.png",
      svg: (
        <svg
          className="h-6 w-5 "
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      ),
      invertedsvg: (
        <svg
          className="h-6 w-5 transform rotate-180"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      ),
    },
    {
      name: "Offers",
      bgColor: "blue-400",
      image: "/images/icon 4.png",
      svg: (
        <svg
          className="h-6 w-5 "
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      ),
      invertedsvg: (
        <svg
          className="h-6 w-5 transform rotate-180"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      ),
    },
    {
      name: "Best Sellers",
      bgColor: "blue-400",
      image: "",
      svg: (
        <svg
          className="h-6 w-5 "
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      ),
      invertedsvg: (
        <svg
          className="h-6 w-5 transform rotate-180"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      ),
    },
    {
      name: "Health Tips",
      bgColor: "blue-400",
      image: "/images/icon 5.png",
      svg: (
        <svg
          className="h-6 w-5 "
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      ),
      invertedsvg: (
        <svg
          className="h-6 w-5 transform rotate-180"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      ),
    },
  ];

  const proteinFood = [
    { title: "Protein food" },
    { title: "protein bars" },
    { title: "protein cookies" },
    { title: "Peanut butter" },
  ];

  const proteins = [
    { title: "whey proteins" },
    { title: "Beginers whey proteins" },
    { title: "Rea whey proteins" },
    { title: "say proteins" },
    { title: "Casein proteins" },
  ];

  const gainers = [
    { title: "Weight Gainers" },
    { title: "Mass Gainers" },
    { title: "Herbal Weight Gainers" },
  ];
  const workout = [
    { title: "Pre-workout" },
    { title: "Creatine" },
    { title: "BCASS" },
    { title: "Card Blends" },
    { title: "Electolytes" },
    { title: "Nitic Oxide" },
    { title: "Other supports" },
  ];
  const skincare = [
    { title: "Carb Blends" },
    { title: "Other supports" },
    { title: " Electolytes" },
    { title: "BCAAs" },
  ];
  const Nutrition = [
    { section1: gainers },
    { section2: workout },
    { section3: proteins },
    { section4: proteinFood },
  ];

  const BodyCare = [
    { section1: skincare },
    { section2: workout },
    { section3: proteins },
    { section4: proteinFood },
  ];

  const renderOptions = (data) => {
    return (
      <div
        className="absolute ml-40 mt-8 bg-white flex flex-row border-2 border-gray-200 px-5 z-10 pt-5"
        onMouseLeave={() => setSublist(false)}
      >
        <div className="w-40 mr-20">
          <h5 className="font-bold mb-10 uppercase">
            {type === "Body Care" ? "Facial skin care" : "Proteins"}
          </h5>
          {data.map((item, key) => (
            <div className="leading-10" key={key}>
              {item.section1 !== undefined &&
                item.section1.map((item1, key1) => (
                  <ul>
                    <li key={key1}>{item1.title}</li>
                  </ul>
                ))}
            </div>
          ))}
        </div>
        <div className="w-40 mr-20">
          <h5 className="font-bold mb-10 uppercase">Gainers</h5>
          {data.map((item, key) => (
            <div className="leading-10" key={key}>
              {item.section2 !== undefined &&
                item.section2.map((item1, key1) => (
                  <ul>
                    <li key={key1}>{item1.title}</li>
                  </ul>
                ))}
            </div>
          ))}
        </div>
        <div className="w-40 mr-20">
          <h5 className="font-bold mb-10 capitalize">Proteins</h5>
          {data.map((item, key) => (
            <div className="leading-10" key={key}>
              {item.section3 !== undefined &&
                item.section3.map((item1, key1) => (
                  <ul>
                    <li key={key1}>{item1.title}</li>
                  </ul>
                ))}
            </div>
          ))}
        </div>
        <div className="w-40 mr-20">
          <h5 className="font-bold mb-10 capitalize">Pre/post workout</h5>
          {data.map((item, key) => (
            <div className="leading-10" key={key}>
              {item.section4 !== undefined &&
                item.section4.map((item1, key1) => (
                  <ul>
                    <li key={key1}>{item1.title}</li>
                  </ul>
                ))}
            </div>
          ))}
        </div>
      </div>
    );
    // }
  };

  const expandDropdown = (type) => {
    if (type === "Categories") {
      setdropDown(true);
      setMainItem(type);
    } else {
      setMainItem("Life");
    }
  };
  const expandSublist = (type) => {
    if (type === "Body Care") {
      setSublist(true);
      setType("Body Care");
    } else if (type === "Sports Nutrition") {
      setSublist(true);
      setType("Sports Nutrition");
    }
  };

  const closeDropdown = () => {
    setdropDown(false);
    setSublist(false);
  };

  return (
    <div
      className="hidden max-w-screen-xl w-full mx-auto md:flex items-center md:flex-row justify-between py-2 px-1"
      onMouseLeave={() => closeDropdown()}
    >
      <div className="flex flex-row  w-5/6">
        {dropDown.map((ele, index) => (
          <div className="md:w-36" key={index}>
            <div className="flex justify-center items-center  relative text-blue-100 hover:text-blue-500">
              <img src={ele.image} className="h-5" />
              <button
                className="font-medium focus:outline-none ml-1 text-xs lg:text-sm "
                onMouseEnter={() => expandDropdown(ele.name)}
                className="font-medium focus:outline-none"
              >
                {ele.name}
              </button>
              {ele.name !== mainItem && ele.svg}
              {ele.name === mainItem && ele.invertedsvg}
            </div>
          </div>
        ))}
        {dropdownOpen && (
          <div className="absolute mt-8 bg-white  border-2  border-r-0 border-gray-200 z-10 ">
            {dropdownContent.map((item, index) => (
              <ul className="text-sm w-full " key={index}>
                <li
                  key={index}
                  className="hover:shadow-md leading-10 mb-3 pl-3 "
                  onMouseEnter={() => expandSublist(item.title)}
                >
                  {item.title}
                </li>
              </ul>
            ))}
          </div>
        )}
        {sublist && type === "Body Care" && renderOptions(BodyCare)}
        {sublist && type === "Sports Nutrition" && renderOptions(Nutrition)}
      </div>
      <div className="w-1/6 flex justify-end">
        <Button
          name="Prescription"
          textSize="xxs"
          mdTextSize="xs"
          fontBold="normal"
          bgColor="blue-600"
          borderRadius="sm"
          mdPx="4"
          mdPy="1"
          textColor="white"
        />
      </div>
    </div>
  );
}
export default Dropdown;
