import React from "react";
import classnames from "classnames";

function OfferBlock({ title, bgColor }) {
  return (
    <>
      <div className="hidden md:block ">
        <div
          className={classnames(
            `shadow hover:shadow-none transition-transform flex justify-between flex-nowrap items-center rounded text-blue-100 bg-${bgColor} md:px-0.5 lg:px-2 md:py-0.5 md:mx-0.5 md:my-0.5  cursor-pointer`
          )}
        >
          <div>
            <img src="/images/icon 1.png" className="h-4" alt="p" />
          </div>
          <p
            className={classnames(
              `md:text-xxxs lg:text-xxs font-medium mx-1 flex flex-nowrap `
            )}
          >
            {title}
          </p>
        </div>
      </div>
    </>
  );
}

export default OfferBlock;
