import React from "react";
import Location from "./Location";
import SearchBar from "../../components/common/SearchBar";

function Search() {
  return (
    <>
      <div className="flex md:justify-between items-center w-full  pr-3.5 py-1 md:p-1 max-w-screen-xl md:mx-auto  ">
        <div className="w-1/6 h-16  md:w-1/4 flex justify-center md:justify-start items-center px-1   ">
          <img
            src="https://www.life-me.com/wp-content/themes/LifePharmacy/assets/images/life-pharmacy-logo.png"
            className="img-responsive pt-1 md:pt-0  h-16 md:h-6 lg:h-8 xl:h-10 "
            alt="Life Pharmacy Logo"
            id="logo"
          ></img>
          <h1 className="hidden md:block text-blue-100 mx-1 md:text-xl lg:text-3xl xl:text-4xl  font-shipporiMincho md:flex items-baseline  flex-nowrap  ">
            lifepharmacy
            <span className="md:text-base lg:text-base xl:text-xl ">
              {" "}
              .com{" "}
            </span>
          </h1>
        </div>
        <div class="w-5/6  md:w-3/5 ">
          <Location />
          <SearchBar />
        </div>
        {/* icons  */}
        <div className="hidden md:block  ">
          <div className=" flex justify-center ">
            <img
              src="/images/icon 8.png"
              alt="P"
              className="h-6 lg:h-7  mx-1 lg:mx-2 bg-gray-200 cursor-pointer shadow hover:shadow-none "
            />
            <img
              src="/images/icon 9.png"
              alt="H"
              className="h-6 lg:h-7  mx-1 lg:mx-2 bg-gray-200 cursor-pointer shadow hover:shadow-none "
            />
            <img
              src="/images/icon 10.png"
              alt="C"
              className="h-6 lg:h-7  mx-1 lg:mx-2 bg-gray-200 cursor-pointer shadow hover:shadow-none "
            />
          </div>
        </div>
      </div>
      <div className="flex justify-between text-xxs font-medium px-3 my-0.5 md:hidden text-blue-100 ">
        <p>Express shipping</p>
        <p>Secure Payments</p>
        <p>Free Shipping above 100</p>
      </div>
    </>
  );
}

export default Search;
