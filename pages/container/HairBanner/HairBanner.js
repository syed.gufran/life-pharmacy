import React from "react";
import Banner2 from "../../components/Banner2";

function Hairbanner() {
  return (
    <div>
      <div className="bg-corousel-bg  md:h-30 w-full my-0 md:my-6">
        <Banner2
          title1="Hair Care"
          offer1="Up to 75% OFF"
          title2="Winter Essential"
          offer2="Up to 75% OFF"
        />
      </div>
    </div>
  );
}

export default Hairbanner;
