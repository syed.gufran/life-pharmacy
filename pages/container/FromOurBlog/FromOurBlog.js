import React from "react";
import CardHeading from ".././../components/common/CardHeading";
import BlogCard from "../../components/BlogCard";

function FromOurBlog() {
  const BlogData = [
    {
      img:
        "https://media.lifepharmacy.com/vday/five-section/Trister_Oximeter.jpg",
      date: "January 10,  2021",
      comment: "10",
      title: "Therapy And Treatment No Longer",
      description:
        "Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.",
    },
    {
      img:
        "https://media.lifepharmacy.com/vday/five-section/Trister_Oximeter.jpg",
      date: "January 10,  2021",
      comment: "10",
      title: "We Make It Awesome",
      description:
        "Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.",
    },
    {
      img:
        "https://media.lifepharmacy.com/vday/five-section/Trister_Oximeter.jpg",
      date: "January 10,  2021",
      comment: "10",
      title: "The Best Pharmacy In The Region",
      description:
        "Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.",
    },
  ];

  return (
    <div className="w-11/12 max-w-screen-xl mx-auto md:border-t md:border-b my-4 md:pt-6 lg:pt-8 border-gray-200">
      <CardHeading title="From Our Blog " />

      <div className="w-full  xl:mx-auto md:gap-3 overflow-x-scroll flex justify-between my-2 lg:my-4  ">
        {BlogData.map((item, index) => (
          <div className="w-9/12 md:w-max flex-none ">
            <BlogCard
              key={index}
              img={item.img}
              date={item.date}
              comment={item.comment}
              title={item.title}
              description={item.description}
            />
          </div>
        ))}
      </div>
    </div>
  );
}

export default FromOurBlog;
