import React from "react";
import EssentialBanner from "../../components/EssentialBanner";

function Essentialbanner1() {
  return (
    <div className="bg-secondary  h-30 w-full my-6 md:my-8 lg:my-10">
      <EssentialBanner title="Daily Essentials 75&#37; OFF" />
    </div>
  );
}

export default Essentialbanner1;
