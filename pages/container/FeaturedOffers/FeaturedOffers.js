import React from "react";
import CardHeading from "../../components/common/CardHeading";
import FeaturedOfferCards from "../FeaturesBlock/FeaturedOfferCards";

function FeaturedOffers() {
  const FeaturedOfferData = [
    { title: "Name" },
    { title: "Name" },
    { title: "Name" },
    { title: "Name" },
    { title: "Name" },
    { title: "Name" },
    { title: "Name" },
    { title: "Name" },
    { title: "Name" },
  ];

  return (
    <div className="md:border md:hidden border-gray-200 rounded-lg bg-gray-50 max-w-screen-xl md:mx-auto px-4 md:px-6 lg:px-8 py-4">
      <CardHeading title="Featured Offers" />
      <div className="grid grid-flow-col grid-cols-3 grid-rows-3 gap-4 my-3 ">
        {FeaturedOfferData.map((item, index) => (
          <FeaturedOfferCards key={index} title={item.title} />
        ))}
      </div>
    </div>
  );
}

export default FeaturedOffers;
