import React from "react";
import classnames from "classnames";

function winterCard({ img, title, borderRight, borderBottom, mdBorderRight }) {
  return (
    <div
      className={classnames(
        `flex flex-col justify-between items-center mx-auto px-4  sm:px-14 md:px-0 py-2 md:py-0  border-${borderRight} md:border-r-0  border-${borderBottom}   md:border-b-0  `
      )}
    >
      <img
        src={img}
        alt="product"
        className={classnames(
          `h-24 sm:h-30 md:h-36 lg:h-48 md:w-full md:py-1 md:my-2  mx-auto md:border-${mdBorderRight}`
        )}
      />
      <p className="my-0.5 text-xxs md:text-base xl:text-lg text-center text-blue-100 font-medium">
        {title}
      </p>
    </div>
  );
}

export default winterCard;
