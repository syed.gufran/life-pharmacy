import React from "react";
import CardHeading from "../../components/common/CardHeading";
import WinterCard from "./WinterCard";
import Carousel, { consts } from "react-elastic-carousel";
import styles from "../../../styles/Corousel.module.css";

function WinterEssentials() {
  const WinterData = [
    {
      img: "/images/Image111.png",
      title: "Manuka Honey",
      borderRight: "r",
      borderBottom: "b",
      mdBorderRight: "r",
    },
    {
      img: "/images/Image112.png",
      title: "Nasal Stick",
      borderRight: "r",
      mdBorderRight: "r",
    },

    {
      img: "/images/Image113.png",
      title: "Lip Balm",
      borderRight: "r",
      borderBottom: "b",
      mdBorderRight: "r",
    },
    {
      img: "/images/Image114.png",
      title: "Sub Category",
      borderRight: "r",
      mdBorderRight: "r",
    },
    {
      img: "/images/Image112.png",
      title: "Nasal Stick",
      borderBottom: "b",
      mdBorderRight: "r",
    },
    {
      img: "/images/Image115.png",
      title: "Sub Category",
    },
  ];

  return (
    <div className="flex flex-row items-center max-w-screen-2xl md:mx-auto my-4 md:my-8 lg:my-10">
      <img
        src="/images/previous.png"
        alt="previous"
        className="hidden md:block shadow-xl rounded-full hover:shadow-none cursor-pointer  "
      />
      <div className="md:border border-gray-200 rounded-lg bg-white max-w-screen-xl md:mx-auto px-4 md:px-6 lg:px-8 py-4 ">
        <CardHeading title="Winter Essentials" />
        <div className="my-4 grid  grid-flow-col grid-cols-3 md:grid-cols-5 grid-rows-2 md:grid-rows-1 ">
          {WinterData.map((item, index) => (
            <WinterCard
              key={index}
              img={item.img}
              title={item.title}
              borderRight={item.borderRight}
              borderBottom={item.borderBottom}
              mdBorderRight={item.mdBorderRight}
            />
          ))}
        </div>
      </div>
      <img
        src="/images/next.png"
        alt="next"
        className="hidden md:block shadow-xl rounded-full hover:shadow-none cursor-pointer "
      />
    </div>
  );
}

export default WinterEssentials;
