import React from "react";
import classnames from "classnames";

function CareProductsCard({ title, bgColor }) {
  return (
    <div
      className={classnames(
        `flex justify-between flex-col  rounded-md  px-2  py-2 md:px-4 md:py-4  bg-${bgColor} bg-cover w-full h-full`
      )}
    >
      {/*Note:-  As of now no title because we already have tiutle in images itself if not then pass title as props inside h5 tag  */}
      <h5 className="text-blue-100 font-medium text-sm md:text-base lg:text-lg xl:text-xl">
        {}
      </h5>

      <div className="flex">
        <p className="duration-300 text-blue-500 text-sm md:text-sm lg:text-base  hover:text-xs md:hover:font-medium   md:hover:text-base lg:hover:text-lg cursor-pointer  ">
          View All
        </p>
        <img src="/images/arrowicon.png" alt="arrow" className="h-5  ml-1" />
      </div>
    </div>
  );
}

export default CareProductsCard;
