import Button from "./common/Button";

function Deals() {
  const data = [
    {
      image: "/images/banner-2-1.png",
      sticker: "/images/call.png",
      bgColor: "blue-200",
      buttonText: "Buy now",
      text: "Bencheck Meter without Strips",
      qty: "Glucose 50s strips",
      name: "Geratherm Thermometer",
      currency: "AED",
      discountPrice: "156.78",
      originalPrice: "236.25",
      nameOnMbl: "Ben check ben check",
    },
    {
      image: "/images/banner-2-2.png",
      sticker: "",
      bgColor: "yellow-100",
      buttonText: "Buy now",
      text: "Sunshine Nutrition manuka honey",
      qty: "830+ Mgo 250 g",
      name: "100% pure NewZealand honey",
      currency: "AED",
      discountPrice: "156.78",
      originalPrice: " 236.25",
      nameOnMbl: "Manuka honey",
    },
  ];

  return (
    <div className="max-w-screen-xl mx-auto  flex flex-col md:flex-row my-10 md:gap-5 lg:gap-8 xl:gap-10">
      {/* <div className ="flex flex-row mx-auto"> */}
      {data.map((ele, index) => (
        //    <div key ={index}>
        <div className="w-full" key={index}>
          <img src={ele.image}></img>
        </div>
        /* <h6 className ="md:hidden text-center capitalize leading-9">{ele.nameOnMbl}</h6>
                        <div className ="md:w-6/12 w-full leading-9">
                        <p className ="hidden md:block">{ele.text}</p>
                        <p className ="hidden md:block">{ele.qty}</p>
                        <p className ="hidden md:block">{ele.name}</p>
                        <div className ="flex flex-row w-full">
                            <div className ="md:w-6/12 w-1/2">
                                <h6> {ele.currency}</h6><p className ="font-bold text-xl">{ele.discountPrice}</p><p className ="hidden md:block"><strike>{ele.originalPrice}</strike></p>
                             </div>
                             <div>
                             <Button name={ele.buttonText} textSize="xs" mdTextSize="base" fontBold="semibold" bgColor="blue-500" px="3" smPx="4" mdPx="7" mdPy="2" py="1" textColor="white" mt="7"  width="1/2" />
                             </div>
                        </div>
                    </div> */
        // </div>
      ))}
      {/* </div>  */}
    </div>
  );
}
export default Deals;
