import React from "react";
import Button from "./common/Button";

function EssentialBanner({ title }) {
  return (
    <div className="px-4 py-4 md:px-24 md:py-6 mx-auto">
      <img src="/images/secondorybanner.png"></img>
    </div>
    // <div className=" max-w-screen-xl mx-auto flex justify-end " >
    // <div className=" w-1/2 sm:w-1/3 md:w-1/2 max-w-screen-xl text-right md:text-center  py-2 sm:py-4 lg:py-8 px-3.5 sm:px-6 md:px-14 lg:px-14 ">
    //     <h1 className="text-lg sm:text-2xl md:text-4xl lg:text-5xl xl:text-7xl md:leading-tight lg:leading-tight xl:leading-tight text-blue-100 font-bold" >{title}</h1>
    //     <Button name="Buy Now" textSize="xs" mdTextSize="base" fontBold="semibold" bgColor="blue-500" px="4" smPx="6" mdPx="10" mdPy="2" py="1" textColor="white" mt="4"  width="1/2" />
    // </div>
    // </div>
  );
}

export default EssentialBanner;
