import React from 'react'

function SearchBar() {
    return (
        <>
            <form method="GET" class="block md:hidden h-12" >
                <div class="relative ">
                <span class="absolute inset-y-0 left-0 flex items-center pl-3 ">
                    <button type="submit" class="p-1 focus:outline-none focus:shadow-outline">
                    <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" viewBox="0 0 24 24" class="w-5 h-5"><path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
                    </button>
                </span>
                <input type="search" name="q" class="py-1 text-sm text-gray-800 bg-gray-100 rounded-md pl-10 focus:pl-16 focus:outline-none focus:bg-gray-100    focus:text-gray-800  " placeholder="Search Medicine &#38; Healthcare Products" autoComplete="off"  />
                </div>
            </form>
        </>
    )
}

export default SearchBar;
