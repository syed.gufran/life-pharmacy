import React from 'react'

function Features({title,description,img}) {
    return (
        <div className="flex flex-row justify-center items-center sm:px-2 lg:px-5 sm:py-2 lg:py-3 lg:my-1  "  >
            <img src={img} alt="logo" class="pr-5 h-7 xl:h-9" />
            <div className="mx-1  leading-3">  
                <h4 className="text-sm text-blue-100 lg:text-sm font-medium leading-3" >{title}</h4>
                <p className=" text-xs text-gray-800 font-normal leading-3" > {description} </p>
            </div>
        </div>
    )
}

export default Features
