import React from "react";

// import Carousel from "react-multi-carousel";
// import "react-multi-carousel/lib/styles.css";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import styles from "../../styles/Corousel.module.css";
function Carousal() {
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 1,
      slidesToSlide: 1,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 1,
      slidesToSlide: 1,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };
  const images = [
    { image: "/images/corouselbanner.png" },
    { image: "/images/corouselbanner.png" },
    { image: "/images/corouselbanner.png" },
  ];
  return (
    <>
      <Carousel
        showThumbs={false}
        showStatus={false}
        showArrows={true}
        showIndicators={true}
        customArrows={true}
        useKeyboardArrows={true}
      >
        <div className="h-48 md:px-28 md:h-96 bg-corousel-bg w-screen">
          <img src="/images/corouselbanner.png" className="object-cover" />
        </div>
        <div className=" h-48 md:px-28 md:h-96 bg-corousel-bg w-screen">
          <img src="/images/corouselbanner.png" className="object-cover" />
        </div>
        <div className=" h-48 md:px-28 md:h-96 bg-corousel-bg w-screen">
          <img src="/images/corouselbanner.png" className="object-cover" />
        </div>
      </Carousel>
      {/*<Carousel
            swipeable={false}
            draggable={false}
            showDots={true}
            centerMode={true}
            responsive={responsive}
            
            ssr={true} // means to render carousel on server-side.
            infinite={true}
            autoPlaySpeed={1000}
            keyBoardControl={true}
            customTransition="all .5"
            transitionDuration={500}
            // containerClass="carousel-container"
            removeArrowOnDeviceType={["tablet", "mobile"]}
            dotListClass="custom-dot-list-style"
            itemClass="carousel-item-padding-40-px"
            className =" px-36 py-24 bg-corousel-bg"
            >
            {/* <div className ="flex flex-row max-w-screen-xl"><div><h1 className ="font-bold text-4xl">Keto</h1><p>Loreum ispum </p></div><div></div>Item 1</div> */}
      {/*<div><img src ="/images/corouselbanner.png" className ="content-center"></img></div>
            <div><img src ="/images/corouselbanner.png"></img></div>
            {/* <div>Item 4</div> */}
      {/* </Carousel>; */}
    </>
  );
}

export default Carousal;
