import React from "react";

function MobileFooter() {
  const footer = [
    { image: "/images/icon 5.png", title: "Home" },
    { image: "/images/icon 3.png", title: "Category" },
    { image: "/images/icon 6.png", title: "Prescription" },
    { image: "/images/icon 8.png", title: "Profile" },
    { image: "/images/icon 10.png", title: "Cart" },
  ];

  return (
    <div className="w-screen md:hidden flex justify-between sticky bottom-0 z-50 shadow-lg border-t-2 bg-white px-5">
      {footer.map((ele, index) => {
        return (
          <div
            key={index}
            className=" flex flex-col justify-center items-center text-xxs font-semibold text-gray-600 hover:text-blue-100 py-2"
            onClick={() =>
              window.scrollTo({ top: 0, left: 0, behavior: "smooth" })
            }
          >
            <img src={ele.image} className="h-6" />
            <p>{ele.title}</p>
          </div>
        );
      })}
    </div>
  );
}
export default MobileFooter;
