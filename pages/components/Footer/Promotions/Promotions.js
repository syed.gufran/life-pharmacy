function Promotions() {
  const promotions = [
    {
      mainTile: "Reliable",
      data:
        "All products displayed from lifepharamcy.com and procured fromverified and licensed pharmacies",
      icon: "/images/footericon.png",
    },
    {
      mainTile: "Secure",
      data:
        "All products displayed from lifepharamcy.com and procured fromverified and licensed pharmacies",
      icon: "/images/footericon.png",
    },
    {
      mainTile: "Support",
      data:
        "Lifepharmacy.com uses secure socket layes(SSL) 128-bitencryption and is payment card Industry Data Security",
      icon: "/images/footericon.png",
    },
  ];
  return (
    <div className=" max-w-screen-2xl hidden md:block container px-4 mx-auto">
      <section className="bg-gray-800  px-10 mx-auto py-10">
        <div className="flex flex-row px-10 mx-auto">
          {promotions.map((ele, index) => (
            <div className="w-4/12 flex flex-row">
              <div className="w-2/12 mr-3 ">
                <img src={ele.icon}></img>
              </div>
              <div className="w-9/12 text-blue-100">
                <p className="text-2xl font-bold">{ele.mainTile}</p>
                <p>{ele.data}</p>
              </div>
            </div>
          ))}
        </div>
      </section>
    </div>
  );
}
export default Promotions;
