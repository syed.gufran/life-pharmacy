function Footer() {
  const data = [
    { maintitle: "25+", title: "Years of trust" },
    { maintitle: "250+", title: "Stores" },
    { maintitle: "25M", title: "Orders Delievered" },
  ];
  const Details = [
    {
      image: "images/warningicon.png",
      maintitle: "Help center",
      site: "help.lifepharamcy.com",
    },
    {
      image: "/images/mail.png",
      maintitle: "Help center",
      site: "care@lifepharmacy.com",
    },
    {
      image: "/images/call.png",
      maintitle: "Phone support",
      site: "+9714571800",
    },
    {
      image: "/images/chat2.png",
      site: "whatus app",
    },
  ];
  return (
    <div className="max-w-screen-2xl md:px-4 mx-auto ">
      <section className="bg-gray-700 px-4 md:py-10 md:px-10 mx-auto">
        <div className="flex flex-row w-full text-center md:text-4xl capitalize py-4 text-blue-100">
          {data.map((ele, index) => (
            <div className="w-4/12" key={index}>
              <p className="font-bold">{ele.maintitle}</p>
              <p className="font-normal text-xs md:text-3xl ">{ele.title}</p>
            </div>
          ))}
        </div>
      </section>

      <section className="hidden sm:hidden md:block bg-gray-50 px-10 mx-auto py-6 text-blue-100">
        <div className="flex flex-row w-full px-10 mx-auto space-x-12">
          <div className="w-4/12 capitalize">
            <p className="font-semibold">We're always Here to help</p>
            <p className="text-xs">
              Reach out to us through any of these support channels
            </p>
          </div>
          {Details.map((item, index) => (
            <div
              className="w-2/12 flex flex-row content-center items-center ml-4 space-x-2"
              key={index}
            >
              <img src={item.image}></img>
              <div className="w-75 m-0">
                <p className="uppercase font-thin text-xs">{item.maintitle}</p>
                <p className="text-xs">{item.site}</p>
              </div>
            </div>
          ))}
          {/* <img src="/images/warningicon.png" className="w-2/12 mr-2"></img>
            <div className="w-8/12 m-0">
              <p className="uppercase font-thin text-sm">Help center</p>
              <p className="font-xs font-semibold">help.lifepharamcy.com</p>
            </div> */}
          {/* </div> */}
          {/* <div className="w-2/12 flex flex-row content-center items-center mr-3">
            <img src="/images/mail.png" className="mr-2 w-2/12"></img>
            <div className="w-8/12 m-0">
              <p className="uppercase font-thin text-sm">Email support</p>
              <p className="font-xs font-semibold">care@lifepharmacy.com</p>
            </div>
          </div>
          <div className="w-2/12 flex flex-row content-center items-center mr-3">
            <img src="/images/call.png" className="mr-2 w-2/12"></img>
            <div className="w-8/12">
              <p className="uppercase font-thin text-sm">Phone support</p>
              <p className="font-xs font-semibold">+9714571800</p>
            </div>
          </div>
          <div className="w-2/12 content-center items-center flex flex-row mr-3">
            <img src="/images/chat2.png" className="mr-2"></img>
            <p className="capitalize font-semibold">whatsapp us</p>
          </div> */}
        </div>
      </section>
    </div>
  );
}
export default Footer;
