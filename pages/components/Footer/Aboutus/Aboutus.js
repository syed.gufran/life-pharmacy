function About() {
  return (
    <div className="max-w-screen-2xl md:px-4 mx-auto">
      <section className="flex md:flex-row  flex-col  flex-col-reverse flex-wrap md:bg-gray-700  md:px-10 mx-auto py-4">
        <div className="flex flex-row md:w-6/12 w-full bg-gray-700 py-4 md:py-0 ">
          <div className="md:w-4/12 w-6/12 md:px-10 px-4 mx-auto  text-blue-100 flex-col-reverse">
            <h6 className="font-semibold">Know us</h6>
            <ul className="leading-9">
              <li>About us</li>
              <li>Contact us</li>
              <li>Press coverage</li>
              <li>Business Paternship</li>
              <li className="md:hidden">Store Locator</li>
              <li>Careers</li>
            </ul>
          </div>
          <div className=" md:block md:w-4/12  w-6/12 md:px-10 px-4 mx-auto capitalize text-blue-100">
            <h6 className=" hidden md:flex font-semibold">Our policies</h6>
            <h6 className="font-semibold md:hidden">Help & Info</h6>
            <ul className="leading-9 ">
              <li className="md:hidden">Account setting</li>
              <li>Privacy policy</li>
              <li>Terms and conditions</li>
              <li>Editorial Policy</li>
              <li>Delivery Polices</li>
              <li>Return policy</li>
            </ul>
          </div>
          <div className="hidden md:block w-4/12 px-10 mx-autocapitalize text-blue-100">
            <h6 className="font-semibold"> Can we help you?</h6>
            <ul className="leading-9">
              <li>Your Account</li>
              <li>Store Locator</li>
              <li>FAQ</li>
            </ul>
          </div>
        </div>
        <div className="flex md:flex-row flex-col md:w-6/12">
          <div className="w-full md:w-6/12  sm:py-3 md:py-0 leading-7 md:leading-0 text-blue-100 sm:px-4 md:px-5 lg:px-5 mx-auto ">
            <h4 className="font-semibold text-center capitalize md:text-left">
              Connect with us
            </h4>
            <div className="flex flex-row w-full md:py-4 py-1 justify-center md:justify-start md:px-0 lg:px-0 mx-auto md:text-left content-center items-center space-x-5 md:space-x-10">
              <img
                src="/images/facebook.png"
                alt="facebook"
                className="pt-2"
              ></img>
              <img
                src="/images/twitter.png"
                alt="twitter"
                className=" pt-2"
              ></img>
              <img
                src="/images/linkedin.png"
                alt="linkedin"
                className="pt-2"
              ></img>
              <img src="/images/insta.png" alt="insta" className="pt-2"></img>
            </div>
            <p className="capitalize text-center md:text-left md:text-sm py-2 md:py-0 md:mb-5">
              Subscribe For the latest discounts & trends
            </p>
            <div className="flex-wrap flex-row  md:w-full w-75 border md:bg-white rounded py-1  mx-4 md:mx-0 mb-2 md:pr-2">
              <input
                className="w-8/12 md:w-8/12 capitalize text-left  text-sm md:text-xs focus:outline-none pl-3 "
                placeholder="Enter your email Address"
              ></input>
              <button className="w-3/12 md:w-4/12 bg-blue-400 rounded py-2 ml-7 md:ml-0 text-sm md:text-xs text-white uppercase">
                Subscribe
              </button>
            </div>
          </div>

          <div className="md:w-6/12  sm:py-3 md:py-0 block text-blue-100  px-4 md:px-5 lg:px-5 mx-auto text-center md:text-left">
            <h5 className="font-semibold mb-4 mt-4 md:mt-0 ">Download App</h5>
            <div className="flex flex-row  content-center items-center mb-10 md:mb-5">
              <img
                alt=""
                src="/images/appstore.png"
                className="w-6/12 mr-2"
              ></img>
              <img
                alt=""
                src="/images/googleplay.png"
                className="w-6/12 mr-2"
              ></img>
            </div>
            <p className="capitalize hidden md:block mb-3">
              Get the link to download
            </p>
            <div className=" hidden md:block flex-wrap flex-row  md:w-full w-75 border md:bg-white rounded py-1  mx-4 md:mx-0 mb-2 md:pr-2">
              <input
                className="w-8/12 md:w-8/12 capitalize text-left  text-sm md:text-xs focus:outline-none pl-3 "
                placeholder="Enter your email Address"
              ></input>
              <button className="w-3/12 md:w-4/12 bg-blue-400 rounded py-2 ml-7 md:ml-0 text-sm md:text-xs text-white uppercase">
                Subscribe
              </button>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
export default About;
