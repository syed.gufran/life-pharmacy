function Privacy() {
  return (
    <div className="max-w-screen-2xl px-4 mx-auto">
      <section className="px-10 mx-auto py-10 text-blue-100">
        <div className="md:flex md:flex-row w-full px-10 md:px-0">
          <p className=" md:w-9/12 px-10 mx-auto hidden md:block">
            © 2021 Life Pharamacy,LLC All rights reserved.
          </p>
          <div className="md:w-3/12 sm:w-full flex flex-row sm:justify-center sm:content-center sm:items-center">
            <img
              src="/images/mastercard.png"
              alt="mastercard"
              className="mr-3 md:h-8"
            ></img>
            <img
              src="/images/visa.png"
              alt="visa"
              className="mr-3 md:h-8"
            ></img>
            <img
              src="/images/american express.png"
              alt="american express"
              className="mr-2 md:h-8"
            ></img>
            <img
              src="/images/COD.png"
              alt="COD"
              className=" hidden md:block mr-2 md:h-8"
            ></img>
          </div>
        </div>
      </section>
    </div>
  );
}
export default Privacy;
