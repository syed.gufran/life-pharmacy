import React from 'react'

function Blog({img,date,comment,title,description}) {
    return (
        <div className="w-11/12 md:w-full flex flex-col justify-between rounded-lg my-1  max-w-sm ">
                {/* Image div  */}
                <img className="w-full h-43  rounded-lg shadow" src={img} alt="blog2" />
 

            <div className="text-xs md:text-base font-normal flex flex-row w-full my-2 divide-x divide-gray-800 ">
                <div className=" flex flex-row w-2/5  items-center ">
                    {/* Date  */}
                    <i className="far fa-calendar text-blue-50 "></i>
                    <p className="mx-2 text-xxs md:text-xs   text-blue-100 ">{date}</p>
                </div>
                <div className="flex  w-2/5 items-center ">
                    {/* comments  */}
                    <i class="far fa-comment mx-2 text-blue-50"></i>
                    <p className="text-xxs md:text-xs  text-blue-100" >{comment} <span> Comments</span> </p>
                </div>
            </div>


            <div className="flex flex-col justify-between">
                {/* description  */}
                <h5 className="text-blue-100 text-sm md:text-lg lg:text-xl font-medium " >{title} </h5>
                <p className="text-gray-400 text-xxs leading-4 md:leading-4 lg:leading-5 md:text-sm lg:text-base  my-2 md:my-3 lg:my-4 w-full">{description}</p>
                <div className="flex flex-row items-center">                  
                    {/* Read more  */}
                    <i class="fas fa-book-open text-sm text-blue-50 hover:text-blue-100"></i>
                    <h5 className="text-blue-50 mx-2 text-sm md:text-base font-normal cursor-pointer hover:text-blue-100 transform transition " >Read More</h5>
                </div>
            </div>
        </div>
    )
}

export default Blog
