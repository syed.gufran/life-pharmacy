import React from "react";

function SearchBar() {
  return (
    <>
      {/* Searchbar */}
      <form method="GET" className="  w-full ">
        <div className="relative mt-1.5 md:mt-0  w-full bg-gray-50 rounded-md  ">
          <span className="absolute inset-y-0 left-0 flex items-center pl-1 md:pl-3 rounded-md  md:pr-1 bg-gray-50  ">
            <button
              type="submit"
              className=" focus:outline-none focus:shadow-outline bg-gray-50 rounded-md "
            >
              <svg
                fill="none"
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                viewBox="0 0 24 24"
                className="w-5 h-5"
              >
                <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
              </svg>
            </button>
          </span>
          <input
            type="search"
            className="z-25 md:hidden text-sm text-blue-900 focus:text-blue-900  cursor-pointer bg-gray-50  rounded-md pl-7 md:pl-10 py-2.5 lg:hover:pl-11  transformation transition duration-500  md:py-1 lg:py-1.5 md:text-base h-full shadow focus:outline-none font-normal w-full "
            placeholder="Search Medicine &#38; Healthcare Products"
            autoComplete="off"
          />
          <input
            type="search"
            className="hidden md:block text-sm md:text-sm  text-blue-900 focus:text-blue-900  cursor-pointer bg-gray-50 placeholder-gray-500  hover:placeholder-opacity-40  rounded-sm pl-7 md:pl-10 py-2.5 lg:hover:pl-11  transformation transition duration-500  md:py-0.5 lg:py-1  h-full shadow focus:outline-none font-normal w-full "
            placeholder="What are youlooking for?"
            autoComplete="off"
          />
        </div>
      </form>
    </>
  );
}

export default SearchBar;
