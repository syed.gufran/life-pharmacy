import React from "react";
import classnames from "classnames";

function CardHeading({ title }) {
  return (
    <div className="flex flex-row justify-between items-center  w-full mx-auto my-1">
      <h4 className="font-normal text-blue-100 md:text-xl lg:text-2xl xl:text-3xl">
        {title}
      </h4>
      <div>
        <button
          className={classnames(
            `bg-blue-500 hover:font-bold  px-1 md:px-5 lg:px-7 md:py-0.5  text-white text-xxs md:text-xs lg:text-sm xl:text-base duration-300 rounded md:rounded-lg hover:rounded-sm md:mt-1 focus:outline-none  transform transition  focus:shadow-md duration-800  md:hover:bg-white border md:hover:border-blue-500 md:hover:text-blue-500 md:hover:text-xxs lg:hover:text-xs md:hover:px-5 lg:hover:px-7 xl:hover:px-9 md:hover:py-0.5 lg:hover:py-1 xl:hover:py-1.5`
          )}
        >
          View All
        </button>
      </div>
    </div>
  );
}

export default CardHeading;
