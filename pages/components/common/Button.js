import React from "react";
import classnames from "classnames";

function Button({
  bgColor,
  textColor,
  name,
  width,
  lgWidth,
  mdWidth,
  textSize,
  mdTextSize,
  lgTextSize,
  xlTextSize,
  mdPx,
  mdPy,
  smPx,
  py,
  px,
  my,
  mx,
  mdMx,
  mt,
  borderRadius,
  borderwidth,

  shadow,
  fontBold,
  borderColor,
}) {
  return (
    <button
      className={classnames(
        `whitespace-nowrap focus:outline-none  transformation duration-300 bg-${bgColor} font-${fontBold} text-${textSize} md:text-${mdTextSize}  lg:text-${lgTextSize} xl:text-${xlTextSize} text-${textColor} width-${width} lg:w-${lgWidth} md:w-${mdWidth} border-${borderwidth} rounded rounded-${borderRadius} border-${borderColor} shadow-${shadow}  font-thin hover:shadow-lg py-${py} sm:px-${smPx}  md:px-${mdPx} md:py-${mdPy} px-${px} my-${my} mx-${mx} md:mx-${mdMx} mt-${mt} border `
      )}
    >
      {name}
    </button>
  );
}

export default Button;
