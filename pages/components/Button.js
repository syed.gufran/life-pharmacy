import React from "react";
import classnames from "classnames";

function Button({
  bgColor,
  textColor,
  name,
  lgWidth,
  mdWidth,
  textSize,
  mdPx,
}) {
  return (
    <button
      className={classnames(
        `focus:outline-none bg-${bgColor} text-${textSize} text-${textColor} lg:w-${lgWidth} md:w-${mdWidth} rounded font-thin hover:shadow-lg sm:py-1 px-1 md:px-${mdPx}  `
      )}
    >
      {name}
    </button>
  );
}

export default Button;
