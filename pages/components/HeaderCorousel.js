import Carousel from "react-elastic-carousel";

const items = [
  { id: 1, image: "/images/corouselbanner.png" },
  { id: 2, image: "/images/corouselbanner.png" },
  { id: 3, image: "/images/corouselbanner.png" },
  { id: 4, image: "/images/corouselbanner.png" },
];

function CorouselExample() {
  return (
    <>
      <Carousel
        itemsToShow={1}
        showEmptySlots={false}
        enableAutoPlay
        autoPlaySpeed={15000}
        className="bg-corousel-bg"
      >
        {items.map((item) => (
          <div
            className="h-48 md:px-28 py-20 md:py-0 md:h-96 bg-corousel-bg w-screen focus:outline-none"
            key={item.id}
          >
            <img src={item.image} className="h-20 md:h-96" />
          </div>
        ))}
        {/* <div className="h-48 md:px-28 md:h-96 bg-corousel-bg w-screen">
          <img src="/images/corouselbanner.png" className="object-cover" />
        </div>
        <div className="h-48 md:px-28 md:h-96 bg-corousel-bg w-screen">
          <img src="/images/corouselbanner.png" className="object-cover" />
        </div> */}
      </Carousel>
    </>
  );
}
export default CorouselExample;
