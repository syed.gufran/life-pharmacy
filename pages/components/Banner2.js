import React from "react";
import Button from "./common/Button";

function Banner2({ title1, title2, offer1, offer2 }) {
  return (
    <div className="md:py-4 md:px-6 lg:py-7 ">
      <div className="flex justify-end md:justify-center md:divide-x-2 md:divide-gray-400 max-w-screen-xl mx-auto">
        <div className="flex flex-col justify-between text-right  text-blue-100  px-3.5 sm:px-6 md:px-14 lg:px-14 ">
          <img src="/images/haircare.png"></img>
          {/* <h1 className="text-lg sm:text-xl md:text-2xl lg:text-3xl xl:text-4xl font-bold  ">{title1}</h1> */}
          {/* <div className="flex justify-end md:my-3" >
                    <h2 className="text-lg sm:text-lg md:text-xl lg:text-2xl xl:text-3xl font-bold md:w-2/3 ">{offer1}</h2>
                </div> */}
          {/* <div>
                    <Button name="Shop Now" textSize="xs" mdTextSize="base" fontBold="semibold" bgColor="blue-500" px="3" smPx="4" mdPx="7" mdPy="2" py="1" textColor="white" mt="7"  width="1/2" />
                </div> */}
        </div>

        <div className=" flex flex-col justify-between text-left  text-blue-100  px-3.5 sm:px-6 md:px-14 lg:px-14 hidden md:block ">
          <img src="/images/tritarybanner2.png"></img>
          {/* <h1 className="text-lg sm:text-xl md:text-2xl lg:text-3xl xl:text-4xl font-bold  ">{title2}</h1> */}
          {/* <div className="flex justify-start md:my-3" >
                    <h2 className="text-lg sm:text-lg md:text-xl lg:text-2xl xl:text-3xl font-bold md:w-1/2 ">{offer2}</h2>
                </div> */}
          {/* <div>
                    <Button name="Shop Now" textSize="xs" mdTextSize="base" fontBold="semibold" bgColor="blue-500" px="3" smPx="4" mdPx="7" mdPy="2" py="1" textColor="white" mt="7"  width="1/2" />
                </div> */}
        </div>
      </div>
    </div>
  );
}

export default Banner2;
