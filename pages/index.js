import Head from "next/head";
import Carousal from "./components/Carousal";
import Footer from "./components/Footer/Services/Services";
import Privacy from "./components/Footer/Privacy/Privacy";
import Promotions from "./components/Footer/Promotions/Promotions";
import About from "./components/Footer/Aboutus/Aboutus";
import Header from "./container/Header/Header";
import FeatureBlock from "./container/FeaturesBlock/FeatureBlock";
import BestSeller from "./container/BestSeller/BestSeller";
import WinterEssentials from "./container/WinterEssentials/WinterEssentials";
import Hairbanner from "./container/HairBanner/HairBanner";
import LookingFor from "./container/LookingFor/LookingFor";
import FromOurBlog from "./container/FromOurBlog/FromOurBlog";
import FeaturedOffers from "./container/FeaturedOffers/FeaturedOffers";
import MoreDetailsButton from "./container/MoreDetailsButton/MoreDetailsButton";
import Essentialbanner1 from "./container/EssentialBanner1/Essentialbanner1";
import Deals from "./components/Deals";
import Recommended from "./container/Recommended/Recommended";
import MobileFooter from "./components/Footer/Footermobile/Footermobile";
import HeaderCorousel from "./components/HeaderCorousel";

export default function Home() {
  return (
    <>
      <Head>
        <title>Life Pharmacy</title>
        <link rel="icon" href="/favicon.ico" />

        <script
          src="https://kit.fontawesome.com/a076d05399.js"
          crossorigin="anonymous"
        ></script>

        <link
          rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.13.0/css/all.css"
          integrity="sha384-Bfad6CLCknfcloXFOyFnlgtENryhrpZCe29RTifKEixXQZ38WheV+i/6YWSzkz3V"
          crossorigin="anonymous"
        />
      </Head>
      <body className="font-poppins">
        {/* Header with searchbar,dropdown, offer-block and logo  */}
        <Header />

        {/* Head Carousal  */}
        <HeaderCorousel />

        {/* <Carousal /> */}

        {/* Features card and tiles  */}
        <FeatureBlock />

        {/* Best Sellers  */}
        <BestSeller />

        {/* Deals  */}
        <Deals />

        {/* Winter Essentials */}
        <WinterEssentials />

        {/* What are you Looking for  */}
        <LookingFor />

        {/* Essential banner  */}
        <Essentialbanner1 />

        {/* Recommended for you products  */}
        <Recommended />

        {/* Winter Essentials  */}
        <WinterEssentials />

        {/* Hair care Banner  */}
        <Hairbanner />

        {/* Featured Offers  */}
        <FeaturedOffers />

        {/* From our blog  */}
        <FromOurBlog />

        {/* More details tab  */}
        <MoreDetailsButton />

        {/* Footer code starts from here */}
        <Footer />
        <About />
        <Promotions />
        <Privacy />
        {/* Footer for Mobile device */}
        <MobileFooter />
      </body>
    </>
  );
}
